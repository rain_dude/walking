use position;
use continuation;

pub enum Action {
    Direction { direction: position::Direction },
    Continuation { continuation: continuation::Continuation, },
}

impl Action {
    pub fn direction(direction: position::Direction) -> Action {
        Action::Direction { direction: direction }
    }

    pub fn continuation(continuation: continuation::Continuation) -> Action {
        Action::Continuation { continuation: continuation }
    }
}
