#[derive(Debug, Copy, Clone)]
pub struct Position {
    pub horizontal: usize,
    pub vertical: usize,
}

impl Position {
    pub fn of(horizontal: usize, vertical: usize) -> Position {
        Position {
            vertical: vertical,
            horizontal: horizontal,
        }
    }

    pub fn move_to(&mut self, direction: &Direction) {
        let moved = self.moved_to(&direction);
        self.vertical = moved.vertical;
        self.horizontal = moved.horizontal;
    }

    pub fn moved_to(&self, direction: &Direction) -> Position {
        match *direction {
            Direction::Up => Position { vertical: self.vertical - 1, ..*self },
            Direction::Down => Position { vertical: self.vertical + 1, ..*self },
            Direction::Left => Position { horizontal: self.horizontal - 1, ..*self },
            Direction::Right => Position { horizontal: self.horizontal + 1, ..*self },
        }
    }
}

pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}
