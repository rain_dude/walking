use position::Position;
use rustbox;
use rustbox::{RustBox, Color};
use point::Point;

pub struct RboxPoint<'wts> {
    position: Position,
    rbox: &'wts RustBox,
}

impl<'wts> Point for RboxPoint<'wts> {
    fn draw(&self, symbol: &str) {
        self.rbox.print(self.position.horizontal,
                        self.position.vertical,
                        rustbox::RB_BOLD,
                        Color::White,
                        Color::Black,
                        symbol);
    }
}

pub struct Factory<'wts> {
    rbox: &'wts RustBox,
}

impl<'wts> Factory<'wts> {
    pub fn for_rust_box(rbox: &'wts RustBox) -> Factory<'wts> {
        Factory { rbox: rbox }
    }

    pub fn at(&self, position: Position) -> RboxPoint<'wts> {
        RboxPoint {
            rbox: self.rbox,
            position: position,
        }
    }
}
