use rustbox::{RustBox, Key, Event};

use walking::Walking;
use boundaries::Boundaries;
use position::{Position, Direction};
use action::Action;
use continuation::Continuation;
use point::Point;
use rbox_point;
use rbox_point::RboxPoint;

pub struct RboxWalking<'wts> {
    rbox: &'wts RustBox,
    walking: Walking,
    points_factory: rbox_point::Factory<'wts>,
}

impl<'wts> RboxWalking<'wts> {
    pub fn draw_boundaries(&self) {
        for vrt in self.walking.vertical_boundaries() {
            self.draw_border_cell_at(self.walking.left_boundary(), vrt);
            self.draw_border_cell_at(self.walking.right_boundary(), vrt);
        }
        for hrz in self.walking.horizontal_boundaries() {
            self.draw_border_cell_at(hrz, self.walking.top_boundary());
            self.draw_border_cell_at(hrz, self.walking.bottom_boundary());
        }
    }

    pub fn draw_border_cell_at(&self, column: usize, row: usize) {
        self.at_coordinates(column, row).draw("#");
    }

    pub fn start(&self) {
        self.at_position(self.walking.position()).draw("o");
        while self.step() != Continuation::Quit {}
    }

    fn step(&self) -> Continuation {
        self.rbox.present();

        let key = match self.read_key() {
            Some(key) => key,
            None => {
                return Continuation::NextIteration;
            }
        };

        let action = self.walking.on_pressed(key);
        match action {
            Action::Continuation { continuation } => continuation,
            Action::Direction { direction } => {
                self.on_move_requested(direction);

                Continuation::NextIteration
            }
        }
    }

    fn read_key(&self) -> Option<Key> {
        match self.rbox.poll_event(false) {
            Ok(Event::KeyEvent(key)) => Some(key),
            Err(e) => panic!("{}", e),
            _ => None,
        }
    }

    fn on_move_requested(&self, direction: Direction) {
        let moved = self.walking.position_moved_to(&direction);
        if self.walking.contains(moved) {
            self.walking.move_position_to(&direction);
            self.clear();
            self.at_position(moved).draw("o");
        }
    }

    fn clear(&self) {
        for horizontal in self.walking.horizontal_inside() {
            for vertical in self.walking.vertical_inside() {
                self.at_coordinates(*horizontal, *vertical).draw(" ");
            }
        }
    }

    pub fn at_coordinates(&self, horizontal: usize, vertical: usize) -> RboxPoint {
        self.at_position(Position::of(horizontal, vertical))
    }

    fn at_position(&self, position: Position) -> RboxPoint {
        self.points_factory.at(position)
    }
}

pub struct Factory {
    rbox: RustBox,
}

impl Factory {
    pub fn new() -> Factory {
        Factory { rbox: Factory::prepare_rust_box() }
    }

    fn prepare_rust_box() -> RustBox {
        RustBox::init(Default::default()).unwrap()
    }

    pub fn restricted_by(&self, boundaries: Boundaries) -> RboxWalking {
        let walking = Walking::restricted_by(boundaries);
        let points_factory = rbox_point::Factory::for_rust_box(&self.rbox);

        let cli = RboxWalking {
            rbox: &self.rbox,
            walking: walking,
            points_factory: points_factory,
        };
        cli.draw_boundaries();

        cli
    }
}
